# Jenkins

## Build imagem Jenkins

```bash
docker build --build-arg=<username-gitlab> --build-arg password=<password-gilab> -t anardy/jenkins:1.0 .
```

Substitua `<username-gitlab>` e `<password-gitlab>` com os dados de usuário e senha que serão criado mais para frente no Gitlab.

## Docker compose up

```bash
docker-compose up -d
```

## Gitlab Config

1) Recupere a senha do usuário `root` do gitlab

```bash
sudo docker exec -it <id-container-gitlab> grep 'Password:' /etc/gitlab/initial_root_password
```

2) Acesse o GitLab em um navegador com a seguinte URL `http://localhost`

3) Com a senha do root faça o login

**Usuário**: root
**Senha**: comando do passo 1

3) Configure um usuário e senhas, com as informações passadas nas variáveis `<username-gitlab>` e `<password-gitlab>` na criação da imagem do jenkins.

Para criar o usuário vá em: Menu > Admin > Overview > Users e clique em `New user`

4) Faça o push dos projetos `shared-devops`, `jobs-devops` e `myapp` para o Gitlab

```bash
git remote add local http://localhost/<usuario>/shared-devops
git push local main
```

```bash
git remote add local http://localhost/<usuario>/job-devops
git push local main
```

```bash
git remote add local http://localhost/<usuario>/app-sample
git push local main
```

## Criar Job app-sample

Há duas maneiras de criar o job no jenkis: manual e por API.

### Manual

Para criação de forma manual siga os passos abaixos:

1) Em um navegador e acesse o Jenkins através da URL `http://localhost:8080`
2) Abra o Job `job-devops`
3) No menu escolha `Build with Parameters`
4) Preencha as variáveis conforme abaixo:

**NAMEJOB**: `app-sample`

**TECHNOLOGY**: `NODEJS`

### Criando job usando API

```bash
curl -X GET http://localhost:8080/crumbIssuer/api/json -u admin:admin
```

```bash
curl -X POST \
-H Jenkins-Crumb:<cumbIssuer> \
http://localhost:8080/job/job-devops/buildWithParameters?token=<token-job> \
--data 'PLATFORM=Passei a descricao do Job via API' \
-u admin:<token-usuário>
```

## Executando pipelie com Shared Library

1. No menu clique em `Build with Parameters`
2. Clique em `Build`

## Referências

https://www.jvt.me/posts/2021/02/23/getting-started-jobdsl-standardised/
