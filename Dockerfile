FROM jenkins/jenkins:lts-jdk11

ARG password
ARG username

RUN mkdir $JENKINS_HOME/casc_configs
COPY ./jenkins.yaml $JENKINS_HOME/casc_configs/jenkins.yaml

ENV CASC_JENKINS_CONFIG=$JENKINS_HOME/casc_configs
ENV JAVA_OPTS=-Djenkins.install.runSetupWizard=false
ENV GITLAB_PASSWORD=${password}
ENV GITLAB_USERNAME=${username}

COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt